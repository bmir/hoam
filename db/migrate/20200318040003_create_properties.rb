class CreateProperties < ActiveRecord::Migration[6.0]
  def change
    create_table :properties do |t|
      t.string :name
      t.references :user, null: false, foreign_key: true
      t.integer :propertyable_id
      t.string :propertyable_type

      t.timestamps
    end
  end
end
