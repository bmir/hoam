class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.references :user, null: false, index: true, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
