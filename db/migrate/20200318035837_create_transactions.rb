class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.integer :whole_number
      t.integer :decimal_number
      t.boolean :type
      t.string :memo
      t.datetime :posted_on

      t.timestamps
    end
  end
end
