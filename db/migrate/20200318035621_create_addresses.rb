class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :country

      t.integer :addressable_id, index: true, foreign_key: {on_delete: :cascade}
      t.string :addressable_type

      t.timestamps
    end
  end
end
