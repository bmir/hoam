class CreateNames < ActiveRecord::Migration[6.0]
  def change
    create_table :names do |t|
      t.string :first
      t.string :middle
      t.string :last
      t.references :profile, null: false, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
