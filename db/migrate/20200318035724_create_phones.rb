class CreatePhones < ActiveRecord::Migration[6.0]
  def change
    create_table :phones do |t|
      t.string :prefix
      t.string :area_code
      t.string :number
      t.references :profile, null: false, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
