Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # Main Navigation Menu
  get '/contact_us', to: 'contact#index'
  get '/pricing', to: 'pricing#index'
  get '/software', to: 'software#index'
  get '/support', to: 'support#index'

  # User menu
  get '/dashboard', to: 'dashboard#index'


  resources :properties do
    collection do
      get :setup
    end
  end
  resources :users do
    collection do
      post :authenticate
      get :sign_in
      post :sign_out
    end
  end

  root to: 'home#index'
end
