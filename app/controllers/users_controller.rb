class UsersController < ApplicationController
  MENU_OPTIONS = %w[dash directory documents finances
                    reports events forms].map(&:capitalize)

  rescue_from ActionController::ParameterMissing do
    logger.error(t 'errors.global.parameter_missing')
    redirect_to sign_in_users_path
  end

  def new
    @user = User.new.tap do |user|
      user.profile = Profile.new.tap do |profile|
        profile.name    = Name.new
        profile.address = Address.new
        profile.phone   = Phone.new
      end
    end
  end

  def sign_in
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      return redirect_to(dashboard_path)
    end

    render :new
  end

  def sign_out
    session[:user_id] = nil
    redirect_to sign_in_users_path
  end

  def authenticate
    @user = User.find_by_email(user_params[:email])

    if @user && @user.authenticate(user_params[:password])
      session[:user_id] = @user.id
      redirect_to dashboard_path
    else
      redirect_to sign_in_users_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation,
                                 profile_attributes:
                                     [name_attributes:    [:first, :last],
                                      address_attributes: [:address1, :address2, :city, :state, :country, :zip_code],
                                      phone_attributes:   [:number]])
  end
end