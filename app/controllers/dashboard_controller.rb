class DashboardController < ApplicationController
  before_action :check_user_authentication

  def index
    @user = User.find(session[:user_id])
  end
end
