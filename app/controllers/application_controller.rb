class ApplicationController < ActionController::Base
  def authenticated?
    @authenticated = session[:user_id].present?
  end

  def check_user_authentication
    redirect_to sign_in_users_path unless authenticated?
  end
end
