module ApplicationHelper
  def label_with_error_info(key, form, model)
    label = form.label key
    return label if model.errors[key].blank?

    label.html_safe + model.errors[key].map { |error_item| '<span class="error-item">' + error_item + '</span>' }.join.html_safe
  end

  def page_header(value)
    ('<div class="page-header"><h1>' +
        value +
        '</h1></div>').html_safe
  end

  def authenticated?
    @authenticated
  end
end
