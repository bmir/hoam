class User < ApplicationRecord
  has_secure_password validations: true
  attr_accessor :password_confirmation

  has_one :profile
  has_many :properties
  accepts_nested_attributes_for :profile

  validates_presence_of :email, :password_digest
  validates_uniqueness_of :email
end
