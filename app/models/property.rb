class Property < ApplicationRecord
  belongs_to :propertyable, polymorphic: true
  belongs_to :user
end
