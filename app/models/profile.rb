class Profile < ApplicationRecord
  belongs_to :user

  has_one :address, as: :addressable
  has_one :name
  has_one :phone

  accepts_nested_attributes_for :name
  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :phone
end
